package main

import (
	"encoding/binary"
	"fmt"
	log "github.com/sirupsen/logrus"
	"net"
	"os"
)

// 发送消息
func sendMessage(conn net.Conn, msg string) {
	// 计算消息长度并发送（4字节的消息头）
	msgLen := uint32(len(msg))
	err := binary.Write(conn, binary.BigEndian, msgLen)
	if err != nil {
		fmt.Println("发送消息长度失败:", err)
	}

	// 发送消息体
	_, err = conn.Write([]byte(msg))
	if err != nil {
		log.Fatal("发送消息体失败:", err)
	}
}

// 接收消息
func receiveMessage(conn net.Conn) string {
	// 读取消息头（4字节，表示消息长度）
	var msgLen uint32
	err := binary.Read(conn, binary.BigEndian, &msgLen)
	if err != nil {
		log.Fatal("读取消息长度失败:", err)
	}

	// 根据消息长度读取消息体
	msg := make([]byte, msgLen)
	_, err = conn.Read(msg)
	if err != nil {
		log.Fatal("读取消息失败:", err)
	}

	return string(msg)
}

func main() {
	// 连接服务器 Dial
	conn, err := net.Dial("tcp", "localhost:8080")
	if err != nil {
		log.Fatal("连接失败:", err)
		os.Exit(1)
	}
	defer conn.Close()

	// 发送消息
	sendMessage(conn, "Hello, Server!")

	// 接收服务器回复
	response := receiveMessage(conn)
	fmt.Println("收到服务器回复:", response)
}
