package main

import (
	"encoding/binary"
	"fmt"
	log "github.com/sirupsen/logrus"
	"net"
	"os"
)

// 处理客户端请求
func handleConnection(conn net.Conn) {
	defer conn.Close()
	for {
		// 读取消息头（4字节，表示消息长度）
		var msgLen uint32
		err := binary.Read(conn, binary.BigEndian, &msgLen)
		if err != nil {
			if err.Error() == "EOF" {
				break // 客户端关闭连接
			}
			log.Println("读取消息长度失败:", err)
			return
		}

		// 根据消息长度读取消息体
		msg := make([]byte, msgLen)
		_, err = conn.Read(msg)
		if err != nil {
			log.Println("读取消息失败:", err)
			return
		}

		// 输出收到的消息
		fmt.Printf("收到消息: %s\n", string(msg))

		// 回复消息
		_, err = conn.Write([]byte("Message received"))
		if err != nil {
			log.Println("发送回复消息失败:", err)
			return
		}
	}
}

func main() {
	// 监听端口
	ln, err := net.Listen("tcp", ":8080")
	if err != nil {
		log.Fatal("监听失败:", err)
		os.Exit(1)
	}
	defer ln.Close()

	fmt.Println("服务器启动，等待客户端连接...")

	// 接受连接
	for {
		conn, err := ln.Accept()
		if err != nil {
			log.Println("连接失败:", err)
			continue
		}
		go handleConnection(conn)
	}
}
