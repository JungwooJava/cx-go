# 注意：如果已经启用了docker，在设置iptables并重启之后会清理掉docker的iptables规则。
# 解决方法：重启iptables之后也重启docker，systemctl restart docker，就可以在iptables中看到docker的规则

# 查看iptables规则和编号
iptables -vnL --line-numbers

# 删除对应chan的规则
iptables -D INPUT N

# 保存配置
service iptables save

# 基础设置
# iptables -P INPUT DROP(不建议使用，默认drop会导致connect超时)
iptables -A INPUT -i lo -j ACCEPT
iptables -A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT

# 允许访问ssh服务的网络(除10.10.2.131外的所有服务器)
#iptables -I INPUT -p tcp -s 10.10.2.0/24 --dport 8022 -j ACCEPT
#iptables -I INPUT -p tcp -s 10.10.1.0/24 --dport 8022 -j ACCEPT
# ssh端口是6802，需要允许网关代理访问(10.10.2.131设置)
iptables -I INPUT -p tcp -s 222.35.252.91 --dport 6802 -j ACCEPT

# 允许访问ftp服务的网络(21端口以及主被动端口)(大数据服务器)
vi /etc/sysconfig/iptables-config
	IPTABLES_MMODULES="ip_nat_ftp"
	lsmod |grep ftp(查询是否加载)
	modprobe ip_nat_ftp(预加载)
iptables -I INPUT -p tcp -s 10.10.2.0/24 --dport 21 -j ACCEPT
iptables -I INPUT -p tcp -s 10.10.1.0/24 --dport 21 -j ACCEPT
# xxh账户使用sftp 6802端口,ip:133.128.42.120(10.10.2.130设置)
iptables -I INPUT -p tcp -s 133.128.42.120 --dport 6802 -j ACCEPT

# 允许访问mysql的网络(大数据服务器)
iptables -I INPUT -p tcp -s 10.10.2.0/24 --dport 3306 -j ACCEPT
iptables -I INPUT -p tcp -s 10.10.1.0/24 --dport 3306 -j ACCEPT

# 允许访问clickhouse的网络(大数据服务器)
iptables -I INPUT -p tcp -s 10.10.2.0/24 --dport 9000 -j ACCEPT
iptables -I INPUT -p tcp -s 10.10.1.0/24 --dport 9000 -j ACCEPT
iptables -I INPUT -p tcp -s 10.10.2.0/24 --dport 8123 -j ACCEPT
iptables -I INPUT -p tcp -s 10.10.1.0/24 --dport 8123 -j ACCEPT
iptables -I INPUT -p tcp -s 10.10.2.0/24 --dport 9444 -j ACCEPT
iptables -I INPUT -p tcp -s 10.10.1.0/24 --dport 9444 -j ACCEPT

# 允许访问snmp的网络(所有设备)
iptables -I INPUT -p udp -s 10.10.2.0/24 --dport 161 -j ACCEPT
iptables -I INPUT -p udp -s 10.10.1.0/24 --dport 161 -j ACCEPT

# 允许访问web和granfana的网络(跳板机131设置)
iptables -I INPUT -p tcp -s 222.35.252.91 --dport 8022 -j ACCEPT
iptables -I INPUT -p tcp -s 222.35.252.91 --dport 8089 -j ACCEPT

# 允许访问ntp的网络(AAA服务器145设置)
iptables -I INPUT -p udp -s 10.10.2.0/24 --dport 123 -j ACCEPT
iptables -I INPUT -p udp -s 10.10.1.0/24 --dport 123 -j ACCEPT

# 允许云网络和漏扫访问(10.10.2.131上设置)
iptables -I INPUT -p tcp -s 10.10.2.158 --dport 8089 -j ACCEPT
iptables -I INPUT -p tcp -s 10.6.0.0/16 --dport 8089 -j ACCEPT

# 允许etl的网络(大数据服务器)
iptables -I INPUT -p tcp -s 10.10.2.0/24 --dport 9663 -j ACCEPT

# 允许influxdb的网络(10.10.2.2-10.10.2.5)
iptables -I INPUT -p tcp -s 10.10.2.0/24 --dport 8086 -j ACCEPT

# 允许DPI服务器访问AAA服务器(10.10.1.145)5901、5902端口
iptables -I INPUT -p tcp -s 10.10.2.0/24 --dport 5901 -j ACCEPT
iptables -I INPUT -p tcp -s 10.10.1.0/24 --dport 5901 -j ACCEPT
iptables -I INPUT -p tcp -s 10.10.2.0/24 --dport 5902 -j ACCEPT
iptables -I INPUT -p tcp -s 10.10.1.0/24 --dport 5902 -j ACCEPT





# 2023-03-21新增规则：
# 允许icmp列表
iptables -I INPUT -p icmp -s 10.10.2.0/24 -j ACCEPT
iptables -I INPUT -p icmp -s 10.10.1.0/24 -j ACCEPT

# 允许ssh列表 （创建自定义链）
iptables -N ssh_permission

iptables -A ssh_permission -s 10.10.2.130 -p tcp --dport 8022  -j ACCEPT
iptables -A ssh_permission -s 10.10.2.131 -p tcp --dport 8022  -j ACCEPT
iptables -A ssh_permission -s 10.10.2.132 -p tcp --dport 8022  -j ACCEPT

iptables -A INPUT -j ssh_permission

# 2023-03-30新增规则：
iptables -A ssh_permission -s 10.6.0.0/16 -p tcp --dport 8022  -j ACCEPT

# (2.131、2.137-138、2.141单独设置6802)
iptables -A ssh_permission -s 10.6.0.0/16 -p tcp --dport 6802  -j ACCEPT

# 2023-10-09新增规则：
# (2.131)
iptables -I INPUT -p tcp -s 10.6.34.46/24 --dport 8022 -j ACCEPT
iptables -I INPUT -p tcp -s 10.6.34.46/24 --dport 8089 -j ACCEPT

# 2023-10-10新增规则：
# (2.131)
iptables -I INPUT -p tcp -s 218.26.126.231 --dport 8022 -j ACCEPT
iptables -I INPUT -p tcp -s 218.26.126.231 --dport 8089 -j ACCEPT
iptables -A ssh_permission -s 218.26.126.231 -p tcp --dport 6802  -j ACCEPT
iptables -I INPUT -p tcp -s 218.26.126.233 --dport 8022 -j ACCEPT
iptables -I INPUT -p tcp -s 218.26.126.233 --dport 8089 -j ACCEPT
iptables -A ssh_permission -s 218.26.126.233 -p tcp --dport 6802  -j ACCEPT



# 默认拒绝，必须放到INPUT链的最后，该条设置要放到最后完成，否则会导致ssh连接不上
iptables -A INPUT -j REJECT --reject-with icmp-host-prohibited

# 2024-01-08 新增规则：
# (2.130)
iptables -I INPUT -p tcp -s 133.128.42.121 --dport 6802 -j ACCEPT
iptables -I INPUT -p tcp -s 133.128.186.27 --dport 6802 -j ACCEPT
iptables -I INPUT -p tcp -s 133.128.186.45 --dport 6802 -j ACCEPT


# 2024-01-09 新增规则：
#（2.131）（-p tcp: 指定这条规则只应用于TCP协议的流量  -s 172.17.0.34: 源地址匹配条件，表示只有源自IP地址为172.17.0.34的流量才会被此规则处理 --dport 6802: 目标端口匹配条件 指明这条规则针对的是目的端口为6802的流量）
iptables -A ssh_permission -p tcp -s 172.17.0.34 --dport 6802 -j ACCEPT
iptables -A ssh_permission -p tcp -s 172.17.0.37 --dport 6802 -j ACCEPT
