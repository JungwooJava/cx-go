package gen_active_code

import (
	"crypto/rand"
	"fmt"
	"math/big"
)

// generateActivationCode 生成一个指定长度的激活码
func generateActivationCode(length int) (string, error) {
	// 定义字符集：包括大写字母、小写字母和数字
	charset := "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
	var activationCode string

	// 从安全随机数生成器中选择字符
	for i := 0; i < length; i++ {
		// 生成一个 0 到 charset 的长度之间的随机数
		index, err := rand.Int(rand.Reader, big.NewInt(int64(len(charset))))
		if err != nil {
			return "", err
		}
		// 将随机数转换为字符，并追加到激活码字符串中
		activationCode += string(charset[index.Int64()])
	}

	return activationCode, nil
}

func main() {
	// 生成一个16位的激活码
	code, err := generateActivationCode(16)
	if err != nil {
		fmt.Println("Error generating activation code:", err)
		return
	}
	fmt.Println("Generated Activation Code:", code)
}
