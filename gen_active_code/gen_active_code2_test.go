package gen_active_code

import (
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"encoding/base64"
	"fmt"
	"io"
	"testing"
)

// pkcs7Padding 对输入的明文进行填充，使其长度是块大小的倍数
func pkcs7Padding(src []byte, blockSize int) []byte {
	padding := blockSize - len(src)%blockSize
	paddingText := bytes.Repeat([]byte{byte(padding)}, padding)
	return append(src, paddingText...)
}

// pkcs7UnPadding 删除填充的字节
func pkcs7UnPadding(src []byte) []byte {
	padding := src[len(src)-1]
	return src[:len(src)-int(padding)]
}

// encryptAES 加密函数，使用 AES 加密算法
func encryptAES(plainText string, key string) (string, error) {
	// 将密钥长度补齐为 32 字节（AES-256）
	keyBytes := []byte(key)
	if len(keyBytes) != 32 {
		return "", fmt.Errorf("key must be 32 bytes")
	}

	// 创建 AES 加密器
	block, err := aes.NewCipher(keyBytes)
	if err != nil {
		return "", err
	}

	// 明文转换为字节切片
	plainTextBytes := []byte(plainText)

	// 填充明文
	plainTextBytes = pkcs7Padding(plainTextBytes, aes.BlockSize)

	// 分配一个足够大的空间来存放密文（包括随机的 IV）
	cipherText := make([]byte, aes.BlockSize+len(plainTextBytes))

	// 随机生成一个初始化向量 (IV)
	iv := cipherText[:aes.BlockSize]
	if _, err := io.ReadFull(rand.Reader, iv); err != nil {
		return "", err
	}

	// 使用 AES 的 CBC 模式加密
	mode := cipher.NewCBCEncrypter(block, iv)
	mode.CryptBlocks(cipherText[aes.BlockSize:], plainTextBytes)

	// 将密文编码为 Base64 格式，便于显示
	return base64.StdEncoding.EncodeToString(cipherText), nil
}

// decryptAES 解密函数
func decryptAES(cipherTextBase64 string, key string) (string, error) {
	// 将密钥长度补齐为 32 字节（AES-256）
	keyBytes := []byte(key)
	if len(keyBytes) != 32 {
		return "", fmt.Errorf("key must be 32 bytes")
	}

	// 解码密文
	cipherText, err := base64.StdEncoding.DecodeString(cipherTextBase64)
	if err != nil {
		return "", err
	}

	// 创建 AES 解密器
	block, err := aes.NewCipher(keyBytes)
	if err != nil {
		return "", err
	}

	// 分离 IV 和密文
	iv := cipherText[:aes.BlockSize]
	cipherText = cipherText[aes.BlockSize:]

	// 使用 AES 的 CBC 模式解密
	mode := cipher.NewCBCDecrypter(block, iv)
	mode.CryptBlocks(cipherText, cipherText)

	// 删除填充
	plainText := pkcs7UnPadding(cipherText)

	// 将解密后的明文转换为字符串
	return string(plainText), nil
}

func TestAes(t *testing.T) {
	// 要加密的明文
	plainText := "2024-12-19"

	// 密钥必须是 32 字节
	key := "thisisaverystrongkey1234567890ab" // 示例密钥
	fmt.Println(len(key))

	key1 := "sinoboycentR&DDepartment3ntav2ge"
	fmt.Println(len(key1))

	// 加密明文
	cipherText, err := encryptAES(plainText, key)
	if err != nil {
		fmt.Println("Error encrypting text:", err)
		return
	}

	// 输出加密后的密文
	fmt.Println("Encrypted text:", cipherText)

	// 解密密文
	decryptedText, err := decryptAES(cipherText, key)
	if err != nil {
		fmt.Println("Error decrypting text:", err)
		return
	}

	// 输出解密后的明文
	fmt.Println("Decrypted text:", decryptedText)
}
