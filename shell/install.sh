#! /bin/bash

workdir=$(pwd)

function install() {
    for v in mysql clickhouse etl env
    do
        if [ -d $workdir/$v ]; then
            cd $workdir/$v
            ./install.sh
        fi
    done
}