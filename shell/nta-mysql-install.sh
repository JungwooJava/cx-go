#! /bin/bash

workdir=$(pwd)

# 配置参数
MYSQL_HOST="localhost"  # MySQL 容器的主机名或IP地址
MYSQL_PORT="3306"       # MySQL 端口号
MYSQL_USER="root"       # MySQL 用户名
MYSQL_PASSWORD="123456" # MySQL 密码
CHECK_INTERVAL=5        # 检查间隔（秒）
CONTAINER_NAME="mysql8" # 容器名称

# 检查 MySQL 连接是否正常
check_mysql_connection() {
   docker exec -it $CONTAINER_NAME mysql -h "$MYSQL_HOST" -P "$MYSQL_PORT" -u"$MYSQL_USER" -p"$MYSQL_PASSWORD" -e "SELECT 1;" > /dev/null 2>&1
}

echo "install mysql."

[ x"$(docker images | grep mysql)" == x""  ] && {
    echo "Error: image mysql not exist."
    exit -1
}

[ x"$(docker ps -a  | grep mysql8)" != x""  ] && {
    docker stop mysql8
    docker rm -f mysql8
}

rm -rf /data/platform/mysql
install -d -m 777 /data/platform/mysql


docker run -d --name mysql8 --privileged=true \
    --restart=always --network=host \
    -v /data/platform/mysql:/var/lib/mysql \
    -v /etc/localtime:/etc/localtime \
    -e MYSQL_ROOT_PASSWORD=123456 \
    -e TZ="Asia/Shanghai" \
    --user=0:0 \
    mysql:latest

# 主循环
sleep 20

docker  cp $workdir/nta_db.sql mysql8:/root/
docker  cp $workdir/nta_service_db.sql mysql8:/root/
docker  cp $workdir/load.sh mysql8:/root/

sleep 5

docker exec mysql8 chmod +x /root/load.sh
docker exec mysql8 /root/load.sh

docker  cp my.cnf mysql8:/etc/my.cnf
docker restart mysql8


