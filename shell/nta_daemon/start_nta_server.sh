#!/bin/bash

# 日志函数
log() {
    echo "$(date '+%Y-%m-%d %H:%M:%S') - $1"
}

# 检查 MySQL 容器是否运行且端口 3306 是否暴露
check_mysql() {
    mysql_container=$(docker ps --filter "name=mysql" --filter "status=running" --quiet)
    if [ -n "$mysql_container" ]; then
        if ss -tuln | grep -qw ":3306"; then
            return 0
        else
            log "MySQL container is running, but port 3306 is not exposed."
            return 1
        fi
    else
        log "MySQL container is not running."
        return 1
    fi
}

# 检查 ClickHouse 容器是否运行且端口 9000 是否暴露
check_clickhouse() {
    clickhouse_container=$(docker ps --filter "name=clickhouse" --filter "status=running" --quiet)
    if [ -n "$clickhouse_container" ]; then
        if ss -tuln | grep -qw ":9000"; then
            return 0
        else
            log "ClickHouse container is running, but port 9000 is not exposed."
            return 1
        fi
    else
        log "ClickHouse container is not running."
        return 1
    fi
}

# 检查服务是否正在运行
check_service_running() {
    local service_name=$1
    systemctl is-active --quiet "$service_name"
    return $?  # 返回服务运行状态
}

# 启动服务，如果未运行则启动；否则跳过
ensure_service_running() {
    local service_name=$1
    if check_service_running "$service_name"; then
        log "$service_name is already running. Skipping restart."
        return 0
    else
        log "$service_name is not running. Starting it now..."
        systemctl restart "$service_name"
        for attempt in {1..12}; do  # 每 5 秒检查一次，最多检查 1 分钟
            if check_service_running "$service_name"; then
                log "$service_name started successfully."
                return 0
            fi
            log "Waiting for $service_name to start... (attempt $attempt)"
            sleep 5
        done
        log "Failed to start $service_name within the timeout period."
        return 1
    fi
}

# 循环检查 MySQL 和 ClickHouse 容器是否都已启动且端口暴露
while true; do
    if check_mysql && check_clickhouse; then
        log "MySQL and ClickHouse containers are both running and ports 3306 and 9000 are exposed. Ensuring services are running..."

        # 确保 nta_server 正常运行
        if ensure_service_running "nta_server"; then
            log "nta_server is running."
        else
            log "Failed to start nta_server. Exiting script."
            #exit 1
        fi

        # 确保 go_etl 正常运行
        if ensure_service_running "go_etl"; then
            log "go_etl is running."
        else
            log "Failed to start go_etl. Exiting script."
            #exit 1
        fi

        log "All services are running. Exiting script."
        #break  # 服务运行状态确认完毕，退出循环
    else
        log "Waiting for MySQL and ClickHouse containers to start and expose ports..."
    fi
    sleep 5
done
