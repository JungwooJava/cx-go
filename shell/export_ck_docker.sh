#!/bin/bash

db=DPI_DB

docker exec -it clickhouse clickhouse-client -u default --password 123456 -q "SHOW TABLES FROM $db" > ./table.txt
echo "--------------------------------${db}"
cat table.txt|while read -r table
do
    if [[ "$table" == ".inner_id."* ]]; then
    echo "skip materialized view $table ($db)"
    continue;
    fi

    echo "export table $table from database $db"

    # dump schema
    docker exec -it clickhouse clickhouse-client -u default --password 123456 -q "SHOW CREATE TABLE DPI_DB.${table} format CSV"|sed "s/DPI_DB\.//g"|sed 's/"CREATE/CREATE/g'|sed 's/"/;/g' > schema.sql
	# docker exec -it clickhouse clickhouse-client -u default --password 123456 --multiquery --query="use DPI_DB;show create table data_security_local;">> schema1.sql
    echo "" >> "schema.sql"
done


# docker exec -it clickhouse clickhouse-client -u default --password 123456 -q "SHOW TABLES FROM DPI_DB" > ./table.txt
# docker exec -it clickhouse clickhouse-client -u default --password 123456 -q "SHOW CREATE TABLE DPI_DB.data_security_local format CSV"|sed "s/DPI_DB\.//g"|sed 's/"CREATE/CREATE/g'|sed 's/"/;/g' >> "schema.sql"
# docker exec -it clickhouse clickhouse-client -u default --password 123456 --query="SHOW CREATE DATABASE DPI_DB" | grep -E '^(CREATE|ATTACH) DATABASE'