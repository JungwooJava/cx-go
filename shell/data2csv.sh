#!/bin/bash

# 手动定义列名并写入CSV文件的第一行
echo '"App Type Name","App ID Name","App Type","App ID","Sum Total"' > output.csv

# 使用 clickhouse-client 查询并将结果追加到 CSV 文件
clickhouse-client -udefault --password=BoyDB2022 --query "select dictGet('bigdata_fcas.app_type_dict','name',app_type),dictGet('bigdata_fcas.app_id_dict','name',app_id), app_type,app_id,formatReadableSize(sum(total)) as sum_total from ( SELECT app_type,app_id , sumMerge(bytes_up_view) + sumMerge(bytes_dn_view) as total FROM bigdata_fcas.dws_user_hour where start_time >= '2025-02-01' and start_time < '2025-03-01' GROUP by app_type,app_id ) GROUP by app_type,app_id order by sum(total) DESC limit 300;" --format CSV > output.csv


