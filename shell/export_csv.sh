#!/bin/bash

# Docker 容器名
DOCKER_CONTAINER_NAME="clickhouse_ftas01"

# 数据库名
DATABASE_NAME="NTA_DB_V2"

# 宿主机存放导出文件的路径
HOST_EXPORT_PATH="./exported_data"

# 创建宿主机目录（如果不存在）
mkdir -p $HOST_EXPORT_PATH

# 需要导出的表名列表
TABLES=("ods_alarm_logs" "another_table" "example_table")  # 在此处添加所有需要导出的表名

# 循环遍历每个表
for TABLE in "${TABLES[@]}"; do
    echo "正在导出表：$TABLE"

    # 通过 Docker exec 运行 ClickHouse 查询并将文件导出到宿主机的指定路径
    docker exec -it $DOCKER_CONTAINER_NAME clickhouse-client --query="SELECT * FROM $DATABASE_NAME.$TABLE LIMIT 10000" --format=CSV > "$HOST_EXPORT_PATH/$TABLE.csv"

    echo "表 $TABLE 数据已导出为 $HOST_EXPORT_PATH/$TABLE.csv"
done

echo "所有表数据导出完成！"
