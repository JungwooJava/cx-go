#!/bin/bash  
  
# 定义Nginx进程名称  
NGINX_PROCESS="nginx"  
  
# 监测Nginx进程是否在运行  
check_nginx() {  
    if nc -z -v -w30 localhost 8090; then
        echo "Nginx is running."  
    else  
        echo "Nginx is not running. Restarting..."  
        # 重新启动Nginx  
        /usr/local/nginx/sbin/nginx -s reload  
    fi
}
  
# 监测并输出结果  
check_nginx

