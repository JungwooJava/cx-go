#!/usr/bin/python
# coding=utf-8

import os
import sys
import getopt

app_id_dict = dict()
app_type_dict = dict()

app_type_file = './app_type.csv'
app_id_file = './app_id.csv'


def proto_list_line_proc(line):
    line_s = line.split(',')
    id_str = line_s[2]
    app_type_str = line_s[5].strip()
    app_id_str = line_s[4]
    app_type = id_str[:2]
    app_id = id_str[:7]

    global app_id_dict
    global app_type_dict
    app_type_dict[app_type] = app_type_str
    app_id_dict[app_id] = app_id_str


def result2file():
    global app_id_dict
    global app_type_dict

    type_file = open(app_type_file, 'w')
    id_file = open(app_id_file, 'w')

    # 首先增加app_type以及app_id都为0的未知应用
    type_file.writelines('00,unknown\n')
    id_file.writelines('0000000,unknown\n')

    for k in sorted(app_type_dict.keys()):
        type_file.writelines(','.join([k, app_type_dict[k]]) + '\n')

    for k in sorted(app_id_dict.keys()):
        id_file.writelines(','.join([k, app_id_dict[k]]) + '\n')

    type_file.close()
    id_file.close()


def usage():
    print "usage:"
    print "-h/--help: for help"
    print "-f: for proto.list file, e.g: -f ./proto.list"


if __name__ == "__main__":
    opts, args = getopt.getopt(sys.argv[1:], "hf:", ["help"])

    org_file = ''
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            usage()
            sys.exit()
        elif opt == '-f':
            org_file = arg

    # 开始解析proto.list
    with open(org_file, 'r') as file:
        lines = file.readlines()
        for line in lines:
            if line.startswith('#'):
                continue
            proto_list_line_proc(line)

    # 解析结果回写文件
    result2file()
