#!/bin/bash

db=$1

clickhouse-client -udefault --password=BoyDB2022  -q "SHOW TABLES FROM $db" > ./table.txt
echo "--------------------------------${db}"
cat table.txt|while read -r table
do
    if [[ "$table" == ".inner_id."* ]]; then
    echo "skip materialized view $table ($db)"
    continue;
    fi

    echo "export table $table from database $db"

    # dump schema
    clickhouse-client -u default --password 123456 -q "SHOW CREATE TABLE ${db}.${table} format CSV"|sed "s/${db}\.//g"|sed 's/"CREATE/CREATE/g'|sed 's/"/;/g' >> "schema.sql"
    echo "" >> "schema.sql"
done
