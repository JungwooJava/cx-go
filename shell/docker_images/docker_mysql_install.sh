
docker run -d --name mysql8 --privileged=true \
    --restart=always --network=host \
    -v /home/data/mysql:/var/lib/mysql \
    -v /etc/localtime:/etc/localtime \
    -e MYSQL_ROOT_PASSWORD=123456 \
    -e TZ="Asia/Shanghai" \
    --user=0:0 \
    mysql: 8.4.0

