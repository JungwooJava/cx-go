#!/bin/bash

# 设置默认日志目录
LOG_DIR="/var/log"

# 设置保留天数
DAYS_TO_KEEP=30

echo "清理 $LOG_DIR 目录下 $DAYS_TO_KEEP 天之前的日志文件..."

# 删除旧的日志文件（`.log` 文件）
find $LOG_DIR -type f -name "*.log" -mtime +$DAYS_TO_KEEP -exec rm -f {} \;

# 删除旧的压缩日志（`.gz` 文件）
find $LOG_DIR -type f -name "*.gz" -mtime +$DAYS_TO_KEEP -exec rm -f {} \;

# 清空特定的日志文件，而不是删除（适合系统运行中某些文件不能删除的情况）
> $LOG_DIR/wtmp
> $LOG_DIR/btmp

echo "日志清理完成！"


# 编辑 cron 任务：
# crontab -e
# 添加以下行，设置每天凌晨 3 点运行脚本
# 0 3 * * * /path/to/clean_logs.sh
