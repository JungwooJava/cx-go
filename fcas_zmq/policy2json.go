package fcas_zmq

import (
	"encoding/json"
	"strings"
	"time"
)

// DpiPolicy 定义对应结构体
type DpiPolicy struct {
	MsgType       int           `json:"msg_type"`
	PolicyID      int           `json:"policy_id"`
	TimeInfo      TimeInfo      `json:"time_info"`
	ControlPolicy ControlPolicy `json:"control_policy"`
	FeatureList   FeatureList   `json:"feature_list"`
	BindObj       string        `json:"bind_obj"`
}

type TimeInfo struct {
	StartTime  string       `json:"start_time"`
	EndTime    string       `json:"end_time"`
	Period     string       `json:"period"`
	PeriodInfo []PeriodInfo `json:"period_info"`
}

type PeriodInfo struct {
	StartPeriod string `json:"start_period"`
	EndPeriod   string `json:"end_period"`
}

type ControlPolicy struct {
	ThresholdUpIDs int   `json:"threshold_up_ids"`
	ThresholdDnIDs int   `json:"threshold_dn_ids"`
	LinkIDs        []int `json:"link_ids"`
}

type FeatureList struct {
	Combined bool          `json:"combined"`
	Tuple    []interface{} `json:"tuple"` // 根据实际类型调整
	AppInfo  []AppInfo     `json:"app_info"`
}

type AppInfo struct {
	AppType int `json:"app_type"`
	AppID   int `json:"app_id"`
}

// 转换函数
func buildPolicyJson(policy *DimControlPolicyEntity, bindFlag string) string {
	// 基础策略结构
	policyData := DpiPolicy{
		MsgType:  1284,
		PolicyID: policy.ID,
		BindObj:  bindFlag,
	}

	// 时间信息处理
	timeInfo := TimeInfo{
		StartTime: formatTime(policy.StartTime),
		EndTime:   formatTime(policy.EndTime),
	}

	// 周期信息处理
	if strings.TrimSpace(policy.PolicyPeriod) == "" {
		timeInfo.Period = "all"
		timeInfo.PeriodInfo = append(timeInfo.PeriodInfo, PeriodInfo{
			StartPeriod: "0",
			EndPeriod:   "0",
		})
	} else {
		switch policy.PeriodType {
		case 1:
			timeInfo.Period = "day"
		case 2:
			timeInfo.Period = "week"
		}

		periods := strings.Split(policy.PolicyPeriod, ",")
		for _, p := range periods {
			parts := strings.Split(p, "-")
			if len(parts) == 2 {
				timeInfo.PeriodInfo = append(timeInfo.PeriodInfo, PeriodInfo{
					StartPeriod: parts[0],
					EndPeriod:   parts[1],
				})
			}
		}
	}

	// 控制策略
	controlPolicy := ControlPolicy{
		ThresholdUpIDs: getThresholdValue(policy.UlFlowRate),
		ThresholdDnIDs: getThresholdValue(policy.DlFlowRate),
		LinkIDs:        getVlanIDs(policy.LinkIds), // 需要实现获取vlan IDs的逻辑
	}

	// 特征列表
	featureList := FeatureList{
		AppInfo: []AppInfo{},
		Tuple:   []interface{}{},
	}

	// 应用信息
	if policy.FlowCtrlType != nil && *policy.FlowCtrlType == 1 && policy.AppTypeId != nil {
		appID := 0
		if policy.AppId != nil {
			appID = *policy.AppId - *policy.AppTypeId*10000
		}
		featureList.AppInfo = append(featureList.AppInfo, AppInfo{
			AppType: *policy.AppTypeId,
			AppID:   appID,
		})
	}

	// 五元组处理（简略示例）
	var targetArray []map[string]string
	if policy.DstIp != "" {
		json.Unmarshal([]byte(policy.DstIp), &targetArray)
	}

	// 合并特征逻辑
	featureList.Combined = len(featureList.AppInfo) > 0 && len(targetArray) > 0

	// 组装最终结构
	policyData.TimeInfo = timeInfo
	policyData.ControlPolicy = controlPolicy
	policyData.FeatureList = featureList

	// 转换为JSON
	jsonData, _ := json.Marshal(policyData)
	return string(jsonData)
}

// 辅助函数：时间格式化
func formatTime(t time.Time) string {
	return t.Format("2006-01-02 15:04:05")
}

// 辅助函数：获取阈值
func getThresholdValue(val *int) int {
	if val == nil {
		return -1
	}
	return *val * 1024
}

// 需要实现的获取vlan IDs的函数（示例）
func getVlanIDs(linkIds string) []int {
	// 实际实现需要查询数据库获取vlan IDs
	return []int{1, 2, 3}
}

// DimControlPolicyEntity 实体结构定义（根据Java类转换）
type DimControlPolicyEntity struct {
	ID           int
	StartTime    time.Time
	EndTime      time.Time
	PolicyPeriod string
	PeriodType   int
	UlFlowRate   *int
	DlFlowRate   *int
	LinkIds      string
	FlowCtrlType *int
	AppTypeId    *int
	AppId        *int
	DstIp        string
	MsgType      int
	// 其他字段省略...
}
