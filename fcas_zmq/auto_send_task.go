package fcas_zmq

import (
	"github.com/jmoiron/sqlx"
	"github.com/robfig/cron/v3"
	log "github.com/sirupsen/logrus"
	"time"
)

type PolicyAutoSendTask struct {
	db        *sqlx.DB
	sendUrl   string
	policyDir string
}

func NewPolicyAutoSendTask(db *sqlx.DB, sendUrl, policyDir string) *PolicyAutoSendTask {
	return &PolicyAutoSendTask{
		db:        db,
		sendUrl:   sendUrl,
		policyDir: policyDir,
	}
}

func (task *PolicyAutoSendTask) run() {
	log.Println("过期策略定时任务 start...")

	// 获取当前时间
	now := time.Now()

	// 查询过期策略
	var expiredPolicies []DimControlPolicyEntity
	err := task.db.Select(&expiredPolicies, "SELECT * FROM dim_control_policy WHERE end_time < ? AND status = ?", now, "BIND")
	if err != nil {
		log.Println("查询过期策略出错:", err)
		return
	}

	for _, policy := range expiredPolicies {
		log.Printf("Expired Policy ID: %d\n", policy.ID)
		policyJson := task.buildPolicyJson(policy, "UNBIND")
		log.Printf("policyJson = %s\n", policyJson)

		// 发送消息
		task.sendMessage(policyJson, task.sendUrl, policy.MsgType)

		// 解绑后，更新策略状态
		_, err := task.db.Exec("UPDATE dim_control_policy SET status = ? WHERE id = ?", "UNBIND", policy.ID)
		if err != nil {
			log.Println("更新策略状态出错:", err)
			continue
		}
		// TODO: 删除策略action
	}

	log.Println("过期策略定时任务 end...")

	log.Println("延期生效策略定时任务 start...")

	// 查询延期策略
	var unBindPolicies []DimControlPolicyEntity
	err = task.db.Select(&unBindPolicies, "SELECT * FROM dim_control_policy WHERE start_time >= ? AND status = ?", now, "UNBIND")
	if err != nil {
		log.Println("查询延期策略出错:", err)
		return
	}

	for _, policy := range unBindPolicies {
		log.Printf("UnBind Policy ID: %d\n", policy.ID)
		bindPolicyJson := task.buildPolicyJson(policy, "BIND")
		log.Printf("unBindPolicyJson = %s\n", bindPolicyJson)

		// 发送消息
		task.sendMessage(bindPolicyJson, task.sendUrl, policy.MsgType)

		// 绑定后，更新策略状态
		_, err := task.db.Exec("UPDATE dim_control_policy SET status = ? WHERE id = ?", "BIND", policy.ID)
		if err != nil {
			log.Println("更新策略状态出错:", err)
		}
	}

	log.Println("延期生效策略定时任务 end...")
}

func (task *PolicyAutoSendTask) buildPolicyJson(policy DimControlPolicyEntity, status string) string {
	// TODO: 实现策略JSON构建逻辑
	return ""
}

func (task *PolicyAutoSendTask) sendMessage(policyJson string, sendUrl string, msgType int) {
	// TODO: 实现消息发送逻辑
}

func main() {
	// 数据库连接
	dsn := "user:password@tcp(127.0.0.1:3306)/dbname?parseTime=true"
	db, err := sqlx.Connect("mysql", dsn)
	if err != nil {
		log.Fatalln(err)
	}

	sendUrl := "http://example.com"
	policyDir := "/path/to/policy/dir"

	task := NewPolicyAutoSendTask(db, sendUrl, policyDir)

	c := cron.New()
	c.AddFunc("@every 1h", task.run) // 每小时执行一次
	c.Start()

	select {} // 防止主线程退出
}
