package avro

import (
	"fmt"
	"github.com/linkedin/goavro"
	"testing"
)

func TestAvro(t *testing.T) {
	// 定义Avro模式
	schemaJSON := `{"type": "record", "name": "User", "fields": [{"name": "name", "type": "string"}, {"name": "age", "type": "int"}]}`
	codec, err := goavro.NewCodec(schemaJSON)
	if err != nil {
		fmt.Println("Error creating Avro codec:", err)
		return
	}

	// 准备数据
	native := map[string]interface{}{
		"name": "Alice",
		"age":  30,
	}

	// 编码数据
	binaryData, err := codec.BinaryFromNative(nil, native)
	if err != nil {
		fmt.Println("Error encoding Avro data:", err)
		return
	}

	fmt.Println("Encoded Avro data:", binaryData)

	// 解码数据
	nativeDecoded, _, err := codec.NativeFromBinary(binaryData)
	if err != nil {
		fmt.Println("Error decoding Avro data:", err)
		return
	}

	fmt.Println("Decoded Avro data:", nativeDecoded)
}
