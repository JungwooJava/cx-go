package main

import (
	"fmt"
	"github.com/lionsoul2014/ip2region/binding/golang/xdb"
	"time"
)

var (
	dbPath = "d:\\ip2region.xdb"
	cBuff  []byte
)

func init() {
	var err error
	cBuff, err = xdb.LoadContentFromFile(dbPath)
	if err != nil {
		fmt.Printf("failed to load content from `%s`: %s\n", dbPath, err)
		return
	}
}

func main() {
	searcher, err := xdb.NewWithBuffer(cBuff)
	if err != nil {
		fmt.Printf("failed to create searcher: %s\n", err.Error())
		return
	}

	defer searcher.Close()

	var ip = "39.156.66.10"
	var tStart = time.Now()
	region, err := searcher.SearchByStr(ip)
	if err != nil {
		fmt.Printf("failed to SearchIP(%s): %s\n", ip, err)
		return
	}

	fmt.Printf("region: %s, took: %s\n", region, time.Since(tStart))
}

// 官方示例
func main1() {
	var dbPath = "ip2region.xdb file path"
	searcher, err := xdb.NewWithFileOnly(dbPath)
	if err != nil {
		fmt.Printf("failed to create searcher: %s\n", err.Error())
		return
	}

	defer searcher.Close()

	// do the search
	var ip = "1.2.3.4"
	var tStart = time.Now()
	region, err := searcher.SearchByStr(ip)
	if err != nil {
		fmt.Printf("failed to SearchIP(%s): %s\n", ip, err)
		return
	}

	fmt.Printf("{region: %s, took: %s}\n", region, time.Since(tStart))

	// 备注：并发使用，每个 goroutine 需要创建一个独立的 searcher 对象。
}

// 如果有nginx 需要这样获取真实IP
// location /api {
//        proxy_set_header X-Forwarded-Proto  $scheme;
//        proxy_set_header X-Forward-For $remote_addr;
//        proxy_set_header X-real-ip $remote_addr;
//        proxy_set_header X-Forwarded-For $remote_addr;
//    }

// func GetRealIP(c *gin.Context) string {
//    return c.Request.Header.Get("X-Forward-For")
//}

// 缓存 VectorIndex 索引
// 可以预先加载 vectorIndex 缓存，然后做成全局变量，每次创建 searcher 的时候使用全局的 vectorIndex，可以减少一次固定的 IO 操作从而加速查询，减少系统 io 压力。
//// 1、从 dbPath 加载 VectorIndex 缓存，把下述 vIndex 变量全局到内存里面。
//vIndex, err := LoadVectorIndexFromFile(dbPath)
//if err != nil {
//    fmt.Printf("failed to load vector index from `%s`: %s\n", dbPath, err)
//    return
//}
//
//// 2、用全局的 vIndex 创建带 VectorIndex 缓存的查询对象。
//searcher, err := xdb.NewWithVectorIndex(dbPath, vIndex)
//if err != nil {
//    fmt.Printf("failed to create searcher with vector index: %s\n", err)
//    return
//}
//
//// 备注：并发使用，全部 goroutine 共享全局的只读 vIndex 缓存，每个 goroutine 创建一个独立的 searcher 对象

// 缓存整个 xdb 数据
// 可以预先加载整个 ip2region.xdb 到内存，完全基于内存查询，类似于之前的 memory search 查询。
// 1、从 dbPath 加载整个 xdb 到内存
//cBuff, err := LoadContentFromFile(dbPath)
//if err != nil {
//fmt.Printf("failed to load content from `%s`: %s\n", dbPath, err)
//return
//}
//
//// 2、用全局的 cBuff 创建完全基于内存的查询对象。
//searcher, err := xdb.NewWithBuffer(cBuff)
//if err != nil {
//fmt.Printf("failed to create searcher with content: %s\n", err)
//return
//}
// 备注：并发使用，用整个 xdb 缓存创建的 searcher 对象可以安全用于并发。
