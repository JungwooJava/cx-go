package date

import (
	"fmt"
	"testing"
	"time"
)

func TestDate(t *testing.T) {
	// 1.UTC时间。协调世界时，又称世界统一时间、世界标准时间。格式如下:
	// Go语言时间类型time.Time是UTC时间格式的
	// 2006-01-02T15:04:05.000Z
	// 2006-01-02 15:04:05 +0000 UTC

	// 2.中国标准时间。 格式如下
	// 2006-01-02 15:04:05
	// 2006-01-12 15:04:05 +0800 CST

	// 3.时间戳 格式如下
	// 1683899356(秒)

	// 创建一个表示UTC时间的Time对象
	utcTime := time.Now().UTC()

	// 输出UTC时间
	fmt.Println("UTC Time:", utcTime.Format(time.RFC3339))

	LocalTime := "2023-05-13 08:17:07"
	//  格式化为当前所在时区的时间
	t1, _ := time.ParseInLocation("2006-01-02 15:04:05", LocalTime, time.Local)
	fmt.Println(t1.Unix())
	//  世界时间
	t2, _ := time.Parse("2006-01-02 15:04:05", LocalTime)
	fmt.Println(t2.Unix())

	// 第一个数据是我们想要的时间

}
