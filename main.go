package main

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func main() {
	r := gin.Default()
	// 301永久重定向
	r.GET("/toBaidu", func(c *gin.Context) {
		c.Redirect(http.StatusMovedPermanently, "https://www.baidu.com")
	})
	// 302临时重定向
	r.GET("/tempRedirect", func(c *gin.Context) {
		c.Redirect(http.StatusFound, "https://www.google.com")
	})
	r.Run(":8080")
}
