package udp

import (
	"fmt"
	"net"
	"strings"
	"testing"
	"time"
)

const (
	serverIP   = "127.0.0.1"
	serverPort = "543"
)

func checkServerStatus() error {
	address := net.JoinHostPort(serverIP, serverPort)
	conn, err := net.DialTimeout("udp", address, 3*time.Second)
	if err != nil {
		return fmt.Errorf("failed to connect: %v", err)
	}
	defer conn.Close()

	testMessage := []byte("<16>Jan  1 00:00:00 testhost testtag[123]: This is a test message")
	_, err = conn.Write(testMessage)
	if err != nil {
		return fmt.Errorf("failed to send message: %v", err)
	}

	buffer := make([]byte, 1024)
	n, err := conn.Read(buffer)
	if err != nil {
		return fmt.Errorf("failed to read response: %v", err)
	}

	response := strings.TrimSpace(string(buffer[:n]))
	if response == "<16>Jan  1 00:00:00 testhost testtag[123]: ACK" {
		return nil
	}

	return fmt.Errorf("unexpected response: %s", response)
}

func TestServerStatus(t *testing.T) {
	err := checkServerStatus()
	if err != nil {
		fmt.Printf("Server is not responding: %v\n", err)
	} else {
		fmt.Println("Server is up and running.")
	}
}
