package main

import (
	"crypto/md5"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"testing"
)

func TestHash(t *testing.T) {
	// 要哈希的原始数据
	data := []byte("Hello, world!")

	// 使用MD5哈希算法
	md5Hash := md5.Sum(data)
	md5HashString := hex.EncodeToString(md5Hash[:])
	fmt.Println("MD5 Hash:", md5HashString)

	// 使用SHA-256哈希算法
	sha256Hash := sha256.Sum256(data)
	sha256HashString := hex.EncodeToString(sha256Hash[:])
	fmt.Println("SHA-256 Hash:", sha256HashString)
}
