package main

import (
	"bytes"
	"compress/gzip"
	"fmt"
	"io/ioutil"
	"testing"
)

func TestGz(t *testing.T) {
	// 原始数据
	data := []byte("Hello, world! This is a test for Gzip compression.")

	// 压缩数据
	var compressed bytes.Buffer
	gzipWriter := gzip.NewWriter(&compressed)
	_, err := gzipWriter.Write(data)
	if err != nil {
		fmt.Println("Error compressing data:", err)
		return
	}
	gzipWriter.Close()

	// 打印压缩后的数据
	fmt.Println("Compressed data:", compressed.Bytes())

	// 解压数据
	gzipReader, err := gzip.NewReader(&compressed)
	if err != nil {
		fmt.Println("Error creating gzip reader:", err)
		return
	}
	decompressed, err := ioutil.ReadAll(gzipReader)
	if err != nil {
		fmt.Println("Error decompressing data:", err)
		return
	}
	gzipReader.Close()

	// 打印解压后的数据
	fmt.Println("Decompressed data:", string(decompressed))
}
